package tobenna.seamfix.test.question1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

/**
 *
 * @author tobe
 */
public class Braces {

    public static void main(String[] args) {
        String[] arry = {"{{[]}{{{}}}}({})", "{()}"};
        System.out.println(Arrays.toString(run(arry)));
    }
    
    public static String[] run(String[] values) {
        return Arrays.stream(values)
                .map(Braces::process)
                .collect(Collectors.toList())
                .toArray(values);
    }

    private static String process(String value) {
        int len = value.length();
        
        // String is not balanced if length is odd number.
        if (len % 2 == 1) {
            return "NO";
        }
        return isBalanced(value.toCharArray(), 0, len - 1) ? "YES" : "NO"; 
    }

    private static boolean isBalanced(char[] tokens, int startIndex, int endIndex) {
        if (startIndex >= endIndex) {
            return true;
        }
        
        char lastToken = tokens[endIndex];
        if (lastToken == '{' || lastToken == '[' || lastToken == '(') {
            return false;
        }
        
        char onePair = tokens[startIndex];
        char otherPair;
        
        switch(onePair) {
            case '{': otherPair = '}'; break;
            case '[': otherPair = ']'; break;
            case '(': otherPair = ')'; break;
            default: return false; // Start brace is invalid
        }
        
        int nextStartIndex = 0;
        boolean balanced = false;
        int duplicate = 0;
        for (int i = startIndex + 1; i <= endIndex; i++) {
            if (onePair == tokens[i]) {
                ++duplicate;
            }
            
            if (tokens[i] == otherPair && duplicate-- == 0) {
                nextStartIndex = i + 1;
                
                if (startIndex + 1 == endIndex) {
                    return true;
                }
                
                if (!(balanced = isBalanced(tokens, startIndex + 1, i - 1))) {
                    return false;
                }
            }
            
            if (balanced) {
                return isBalanced(tokens, nextStartIndex, endIndex);
            }
        }
        return balanced;
    }
}