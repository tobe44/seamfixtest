package tobenna.seamfix.test.question6;

import java.util.Arrays;

/**
 *
 * @author tobe
 */
public class Duplicate {
    
    public static int countDuplicates(int[] numbers) {
        int count = 0;
        int currentNumber = numbers[0];
        boolean notCounted = true;
        Arrays.sort(numbers);
        
        for (int i = 1, len = numbers.length; i < len; i++) {
            if (currentNumber == numbers[i]) {
                if (notCounted) {
                    count++;
                    notCounted = false;
                }
            } else {
                currentNumber = numbers[i];
                notCounted = true;
            }
        }
        return count;
    }
    
    public static void main(String[] args) {
        int[] a = {1,3,1,4,5,5,5,2, 9,6,2,4,1,7,9,0,3,11,323,4,87,53,4,3};
        System.out.println(countDuplicates(a));
    }
}