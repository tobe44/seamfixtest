package tobenna.seamfix.test.question2;

import java.util.Scanner;

/**
 *
 * @author tobe
 */
public class MyCalculator {
    
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        while(true) {
            String[] next = input.nextLine().split(" ");

            int n = Integer.parseInt(next[0]);
            int p = Integer.parseInt(next[1]);

            try {
                int power = calcPower(n, p);
                System.out.println(power);
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }
    }
    
    public static int calcPower(int n, int p) throws Exception {
        if ( n < 0 || p < 0 ) {
            throw new Exception("n and p should be non-negetive");
        }
        return (int) Math.pow(n, p);
    }
}
