package tobenna.seamfix.test.question4;

import java.math.BigInteger;

/**
 *
 * @author tobe
 */
public class Fibonacci {
    
    public static void main(String[] args) {
        System.out.println(computeFibonacci(100));
    }
    
    public static BigInteger computeFibonacci(int n) {
        return n == 1 ? BigInteger.ZERO : fib(BigInteger.ZERO, BigInteger.ONE, n);
    }
    
    private static BigInteger fib(BigInteger a, BigInteger b, int n) {
        return n == 2 ? b : fib(b, a.add(b), --n);
    }
}