package tobenna.seamfix.test.question7;

import java.util.HashMap;
import java.util.Scanner;

/**
 *
 * @author tobe
 */
public class Pairs {
    
    public static int pairCount(int[] numbers, int diff) {
        int count = 0;
        HashMap<Integer, Integer> numbers2 = new HashMap<>(numbers.length);
        
        for (int i = 0; i < numbers.length; i++) {
            numbers2.put(numbers[i], i);
        }
        
        for (int i = 0; i < numbers.length; i++) {
            if (numbers2.get(numbers[i] + diff) != null) {
                count++;
            }
        }
        return count;
    }
    
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int arraySize = input.nextInt();
        int difference = input.nextInt();
        input.nextLine();
        int[] numbers = new int[arraySize];
      
        for(int i = 0; i < numbers.length; i++) {
            numbers[i] = input.nextInt();
        }
        
        System.out.println("\n\n");
        System.out.println(pairCount(numbers, difference));
    }
}
