package tobenna.seamfix.test.question5;

import java.util.Scanner;

/**
 *
 * @author tobe
 */
public class Similarity {
    
    public static int getSimilarity(String s) {
        int length = s.length();
        int sum = length; // Because the string is its first suffix.
        
        for (int i = 1; i < length; i++) {
            int start = 0;
            int j = i;
            
            while(j < length) {
                if (s.charAt(j++) == s.charAt(start++)) {
                    ++sum;
                } else {
                    break;
                }
            }
        }
        return sum;
    }
    
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int size = input.nextInt();
        input.nextLine();
        
        // If size is out of bounds, the program terminates quietly
        if (size > 0 && size <= 10 ) {
            int[] results = new int[size];

            // New input is evaluated and tucked away in result
            // before prompting for next input
            for (int i = 0; i < size; i++) {
                String value = input.nextLine();
                results[i] = getSimilarity(value);
            }
            
            // Print result
            for (int result: results) {
                System.out.println(result);
            }
        }
    }
}
