package tobenna.seamfix.test.question3;

/**
 *
 * @author tobe
 */
public abstract class Arithmetic {
    abstract void add(int a, int b);
    
    public String getSimpleName() {
        return Arithmetic.class.getSimpleName();
    }
}
